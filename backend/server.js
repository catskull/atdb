var http = require('http');

http.createServer(function(req, res) {
    var url = req.url;
    console.log('Recieving request on ' + url);

    var callback = function(err, result) {
        res.writeHead(200, {
            'Content-Type' : 'x-application/json'
        });
        // console.log('json:', result);
        res.end(result);
    };

    if(url == '/atcommands.json')
    {
      getSQL(callback, 'select * from linkedTable inner join atCommands on linkedTable.atCommands_id=atCommands.id left outer join names on linkedTable.names_id=names.id left outer join descriptions on linkedTable.descriptions_id=descriptions.id left outer join ranges on linkedTable.ranges_id=ranges.id left outer join defaults on linkedTable.defaults_id=defaults.id left outer join targets on linkedTable.targets_id=targets.id left outer join products on linkedTable.products_id=products.id left outer join versions on linkedTable.versions_id=versions.id')
    }

    else if(url == '/products.json')
    {
      getSQL(callback, 'select * from products')
    }

}).listen(3000);

function getSQL(callback, query) {
    var mysql = require('mysql');
    var connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        database : 'atdb',
    });

    connection.connect();
    var json = '';
    var query = query;
    connection.query(query, function(err, results, fields) {
        if (err)
            return callback(err, null);

        // console.log('The query-result is: ', results[0]);

        for(var i in results)
        {
            delete results[i].atCommands_id;
            delete results[i].names_id;
            delete results[i].descriptions_id;
            delete results[i].ranges_id;
            delete results[i].defaults_id;
            delete results[i].dataTypes_id;
            delete results[i].targets_id;
            delete results[i].products_id;
            delete results[i].xctuDisplayModes_id;
            delete results[i].versions_id;
            delete results[i].categories_id;
        }

        json = JSON.stringify(results);

        connection.end();
        // console.log('JSON-result:', json);
        callback(null, json);
    });
};
